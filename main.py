import sys
from PyQt5.QtWidgets import QMainWindow,QApplication, QWidget,QCheckBox
from GUI.update import Ui_MainWindow


class MyWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.setupUi(self)
        self.SelfIPScan()

    def SelfIPScan(self):
        row = self.tableWidget.rowCount()
        print(row)
        self.tableWidget.setRowCount(row+1)
        ck = QCheckBox()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())
